<?php
defined('BASEPATH') or exit('No direct script access allowed');
class CountryModel extends CI_Model
{
    public function getCountry()
    {
        return $this->db->get("country");
    }

    public function getCity()
    {
        return $this->db->get("city");
    }
}

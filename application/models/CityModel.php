<?php
defined('BASEPATH') or exit('No direct script access allowed');
class CityModel extends CI_Model
{
    public function getCity()
    {
        return $this->db->get("city");
    }
}

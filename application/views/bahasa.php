<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Daftar bahasa</title>

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/jquery.dataTables.css">
    <script src="<?php echo base_url() ?>assets/jquery-3.2.1.slim.min.js"></script>
    <script type="text/javascript" charset="utf8" src="<?php echo base_url() ?>assets/jquery.dataTables.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
</head>

<body>


    <div class="row justify-content-center pt-4">
        <div class="card text-black bg-white border-dark mb-3" style="max-width: 700px;">
            <div class="card-header border-dark bg-dark">
                <h1 class="text-center text-white">DAFTAR BAHASA</h1>
            </div>
            <div class="card-body text-center">
                <?php
                $template = array(
                    'table_open'  => '<table id="myTable" border="1" class="table table-striped">',
                    'thead_open'            => '<thead  class="thead thead-dark">',
                    'thead_close'           => '</thead>',
                    'tbody_open'            => '<tbody>',
                    'tbody_close'           => '</tbody>'
                );
                $this->table->set_template($template);
                $this->table->set_heading("langauge");
                foreach ($Language->result() as $r) {
                    $this->table->add_row($r->Language);
                }
                echo $this->table->generate();
                ?> </div>
            <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
                <div class="collapse navbar-collapse align-content-center" id="navbarSupportedContent">
                    <ul class="navbar-nav mr-auto">
                        <li class="nav-item ">
                            <a class="nav-link" href="<?php echo site_url() ?>/country">DAFTAR NEGARA <span class="sr-only">(current)</span></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="<?php echo site_url() ?>/city ">DAFTAR KOTA <span class="sr-only">(current)</span></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link active" href="<?php echo site_url() ?>/bahasa">DAFTAR BAHASA <span class="sr-only">(current)</span></a>
                        </li>
                    </ul>
                </div>
            </nav>
        </div>

        <!-- jQuery first, then Popper.js, then Bootstrap JS -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
        <script>
            $(document).ready(function() {
                $('#myTable').DataTable();
            });
        </script>
</body>
</body>

</html>
<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Country extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model("CountryModel", "", TRUE);
    }
    public function index()
    {
        $data['country'] = $this->CountryModel->getCountry();
        $this->load->view("country", $data);
    }
}

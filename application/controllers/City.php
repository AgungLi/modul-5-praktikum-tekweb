<?php
defined('BASEPATH') or exit('No direct script access allowed');
class City extends CI_Controller
{
    public function index()
    {
        $this->load->model("CityModel", "", TRUE);
        $data['City'] = $this->CityModel->getCity();
        $this->load->view("city", $data);
    }
}
